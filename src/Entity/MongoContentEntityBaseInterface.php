<?php

namespace Drupal\mcet\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use MongoDB\BSON\ObjectId;

interface MongoContentEntityBaseInterface extends ContentEntityInterface {
    /**
     * @return array indexes that will be indexed in mongo
     */
    public function getIndexes();

  /**
   * @return ObjectId
   */
    public function mongoId();
}

<?php

namespace Drupal\mcet\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use MongoDB\BSON\ObjectId;

abstract class MongoContentEntityBase extends ContentEntityBase implements MongoContentEntityBaseInterface {

  protected $fieldsToIndex = [];

  public function __construct(array $values, $entity_type, $bundle = FALSE, $translations = []) {
    parent::__construct($values, $entity_type, $bundle, $translations);

    $this->fieldsToIndex = $this->getEntityType()->get('mongo_indexes_fields');
  }

  public function mongoId() {
    $id = parent::id();
    return new ObjectId($id);
  }

  public function getIndexes() {
    return $this->fieldsToIndex;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Mongo ID'));

    return $fields;
  }
}

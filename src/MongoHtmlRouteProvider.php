<?php

namespace Drupal\mcet;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides HTML routes for entities with administrative add/edit/delete pages.
 *
 * Use this class if the add/edit/delete form routes should use the
 * administrative theme.
 *
 * @see \Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider.
 */
class MongoHtmlRouteProvider extends AdminHtmlRouteProvider {
    /**
     * Gets the canonical route.
     *
     * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
     *   The entity type.
     *
     * @return \Symfony\Component\Routing\Route|null
     *   The generated route, if available.
     */
    protected function getCanonicalRoute(EntityTypeInterface $entity_type) {
        $route = parent::getCanonicalRoute($entity_type);

        if ($route && $this->getEntityTypeIdKeyType($entity_type) === 'string') {
            $route->setRequirement($entity_type->id(), '.*?');
        }
        return $route;
    }

    /**
     * Gets the edit-form route.
     *
     * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
     *   The entity type.
     *
     * @return \Symfony\Component\Routing\Route|null
     *   The generated route, if available.
     */
    protected function getEditFormRoute(EntityTypeInterface $entity_type) {
        $route = parent::getEditFormRoute($entity_type);

        if ($route && $this->getEntityTypeIdKeyType($entity_type) === 'string') {
            $route->setRequirement($entity_type->id(), '.*?');
        }
        return $route;
    }

    /**
     * Gets the delete-form route.
     *
     * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
     *   The entity type.
     *
     * @return \Symfony\Component\Routing\Route|null
     *   The generated route, if available.
     */
    protected function getDeleteFormRoute(EntityTypeInterface $entity_type) {
        $route = parent::getDeleteFormRoute($entity_type);

        if ($route && $this->getEntityTypeIdKeyType($entity_type) === 'string') {
            $route->setRequirement($entity_type->id(), '.*?');
        }
        return $route;
    }
}

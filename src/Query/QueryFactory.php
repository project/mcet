<?php

namespace Drupal\mcet\Query;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\Query\QueryBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Query\QueryFactoryInterface;
use Symfony\Component\Serializer\Serializer as SymfonySerializer;
use \Drupal\Core\Cache\CacheBackendInterface;
use Drupal\mcet\Normalizer\Normalizer as MongoNormalizer;

class QueryFactory implements QueryFactoryInterface {
  const DB_NAME = 'entities';

  /** @var MongoDb\Database Database */
  public $database;

  /** @var \Drupal\mongodb\DatabaseFactory $mongodbFactory */
  public $factory;

  /** @var \Symfony\Component\Serializer\Serializer $serializer */
  public $serializer;

  /** @var MongoNormalizer $normalizer */
  public $normalizer;

  /**
   * Cache backend.
   *
   * @var CacheBackendInterface
   */
  protected $cache;

  public $namespaces;

  public function __construct(\Drupal\mongodb\DatabaseFactory $factory, SymfonySerializer $serializer, MongoNormalizer $normalizer, CacheBackendInterface $cache) {
    $this->factory = $factory;
    $this->serializer = $serializer;
    $this->normalizer = $normalizer;
    $this->namespaces = QueryBase::getNamespaces($this);
    $this->cache = $cache;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('mongodb.database_factory'),
      $container->get('serializer'),
      $container->get('mcet.normalizer'),
      $container->get('entity.memory_cache')
    );
  }

  public function getDatabase($db_name) {
    if (!$this->database) {
      $this->database = $this->factory->get($db_name);
    }

    return $this->database;
  }

  public function get(EntityTypeInterface $entity_type, $conjunction) {
    $class = QueryBase::getClass($this->namespaces, 'Query');
    return new $class($entity_type, $conjunction, $this->namespaces, $this->factory, $this->serializer, $this->cache);
  }

  public function getAggregate(EntityTypeInterface $entity_type, $conjunction) {
    $class = QueryBase::getClass($this->namespaces, 'QueryAggregate');
    return new $class($entity_type, $conjunction, $this->namespaces, $this->factory, $this->serializer, $this->cache);
  }

}

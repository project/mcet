<?php

/**
 * @file
 * Contains \Drupal\external_entities\Entity\Query\External\Query.
 */

namespace Drupal\mcet\Query;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Query\QueryBase;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\mcet\Entity\MongoContentEntityBaseInterface;
use Drupal\mongodb\DatabaseFactory;
use MongoDB\Model\BSONDocument;
use MongoDB\Model\IndexInfo;
use MongoDB\Operation\Explain;
use MongoDB\Operation\Explainable;
use Symfony\Component\Serializer\Serializer;
use MongoDB\Driver\Exception\CommandException;

/**
 * The SQL storage entity query class.
 */
class Query extends QueryBase implements QueryInterface {

  protected const CACHE_CID_PREFIX = 'mongo_indexed_';

  /** @var \Drupal\mongodb\DatabaseFactory $mongodbFactory */
  public $factory;

  /** @var MongoDb\Database Database */
  public $database;

  /** @var \MongoDB\Collection */
  private $collection = null;

  /** @var \Symfony\Component\Serializer\Serializer $serializer */
  public $serializer;

  public $filters = [];
  protected $options = [];
  protected $projection = [];

  public $fieldDefinitions = [];

  /** @var MongoContentEntityBaseInterface null */
  public $entityInstance = null;
  public $entity_id = null;
  public $bundle = null;

  public $returnDocuments = FALSE;

  /**
   * Cache backend.
   *
   * @var CacheBackendInterface
   */
  protected $cache;

  /**
   * Constructs a query object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   * @param string $conjunction
   * @param array $namespaces
   * @param DatabaseFactory $factory
   * @param Serializer $serializer
   * @param CacheBackendInterface $cache
   */
  public function __construct(EntityTypeInterface $entity_type, $conjunction, array $namespaces, DatabaseFactory $factory, Serializer $serializer, CacheBackendInterface $cache) {
    parent::__construct($entity_type, $conjunction, $namespaces);
    $this->factory = $factory;
    $this->serializer = $serializer;
    $this->cache = $cache;
  }


  /**
   * {@inheritdoc}
   */
  public function aggregate($stage, $logic, $langcode = NULL, &$alias = NULL) {
    $this->aggregate[($stage . "_" . sizeof($this->aggregate))] = [
      'stage' => $stage,
      'logic' => $logic,
      'langcode' => $langcode,
    ];

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    return $this
      ->prepare()
      ->compile()
      ->addSortToOptions()
      ->result();
  }

  /**
   * @param Explainable $operation
   * @return array|\MongoDB\Collection|object
   */
  public function explain($operation) {
    return $this->getCollection()->explain($operation, $this->options);
  }

  /**
   * @param bool $flag
   * @return $this
   */
  public function setExplain(bool $flag, $verbosity = Explain::VERBOSITY_QUERY): QueryAggregate {
    $this->options['explain'] = $flag;
    $this->options['verbosity'] = $verbosity;
    return $this;
  }

  public function setProjection(array $projection): Query {
    $this->projection = $projection;
    return $this;
  }

  /**
   * @param $filter
   * @param array|null $options
   * @return int
   */
  public function deleteMany($filter, array $options = []) {
    $result = $this->getCollection()->deleteMany($filter, $options);
    return $result->getDeletedCount();
  }

  /**
   * @param array|null $options
   * @return array|object
   */
  public function drop(array $options = []) {
    return $this->getCollection()->drop($options);
  }

  protected function addSortToOptions() {
    if ($this->count) {
      $this->sort = [];
    }

    $sort = [];
    if ($this->sort) {
      foreach ($this->sort as $key => $data) {
        $sort[$data['field']] = $data['direction'] === 'DESC' ? -1 : 1;
      }
    }

    $this->options = array_merge($this->options, [
      'sort' => $sort
    ]);

    return $this;
  }

  public function fetchDocuments() {
    $this->returnDocuments = TRUE;
//    $this->projection = $projection;
    return $this;
  }

  protected function prepare() {
    $this->setOptions();

    $this->entity_id = $this->getEntityTypeId();
    $this->bundle = $this->getEntityTypeId();

    $this->fieldDefinitions = \Drupal::entityManager()->getFieldDefinitions($this->entity_id, $this->bundle);

    $entityClass = $this->entityType->getClass();
    $this->entityInstance = new $entityClass([], $this->entity_id, $this->bundle);

    if (method_exists($this->entityInstance, 'getIndexes')) {
      $this->createIndexes();
    }

    return $this;
  }

  /**
   * @throws \Exception
   */
  protected function createIndexes() {
    $cid = static::CACHE_CID_PREFIX . $this->getEntityTypeId();
    if ($this->cache->get($cid)) {
      return;
    }

    $requiredIndexes = $this->entityInstance->getIndexes();
    if (is_null($requiredIndexes)) {
      return;
    }

    $cursor = $this->getCollection()->listIndexes();

    $existingIndexes = [];
    foreach ($cursor as $index) {
      if ($index->getName() != '_id_') {
        $existingIndexes[] = $index;
      }
    }

    try {
      foreach ($requiredIndexes as $type => $fields) {
        switch ($type) {
          case 'string_indexes':
            $this->indexTextFields($fields, $existingIndexes);
            break;
          case 'numeric_indexes':
            $this->indexFields($fields, $existingIndexes, false);
            break;
          case 'compound_indexes':
            $this->indexFields($fields, $existingIndexes, true);
            break;
        }
      }
    } catch (CommandException $e) {
      throw $e;
    }

    $this->cache->set($cid, 1);
  }

  /**
   * @param $fields
   * @param IndexInfo[] $existingIndexes
   * @param $isCompound
   * @return void
   */
  private function indexFields($fields, $existingIndexes, $isCompound) {
    $newIndexes = [];
    $indexesToDrop = [];

    foreach ($fields as $field_name => $field_values) {
      $field_values = $isCompound ? $field_values['fields'] : $field_values;
      $key = $this->findMatchingOrChangedIndexKey($existingIndexes, $field_name, $field_values);
      if ($key < 0) {
        $newIndexes[] = [
          'key' => !$isCompound ? $this->buildNumericIndexFieldValue($field_name, $field_values) : $field_values,
          'name' => $field_name,
        ];
      } else {
        unset($existingIndexes[$key]);
      }
    }

    foreach ($existingIndexes as $key => $index) {
      if ((!$isCompound && sizeof($index->getKey()) === 1) || ($isCompound && sizeof($index->getKey()) > 1)) {
        $indexesToDrop[$key] = $index;
      }
    }

    if (!empty($indexesToDrop)) {
      $this->dropIndexes($indexesToDrop);
    }

    if (!empty($newIndexes)) {
      $this->getCollection()->createIndexes($newIndexes);
    }
  }

  /**
   * @param $data
   * @param IndexInfo[] $existingIndexes
   * @return void
   */
  private function indexTextFields($data, $existingIndexes) {
    /** @var \MongoDB\Model\IndexInfo $textIndex */
    $textIndex = array_filter(array_map(function ($index) use ($data, $existingIndexes) {
      /** @var \MongoDB\Model\IndexInfo $index */

      $keys = array_keys($index->getKey());
      if (in_array($keys[0], ['_fts', '_ftx'])) {
        return $index;
      }
    }, $existingIndexes), function ($value) {
      return !is_null($value);
    });

    if (!empty($textIndex)) {
      $textIndex = array_values($textIndex)[0];
    }

    $textFields = [];
    if (!empty($textIndex)) {
      $textFields = $textIndex->offsetGet('weights');
    }

    $newIndexes = [];
    $indexDropped = false;
    foreach ($data as $field_name => $field_value) {
      $newIndexes = array_merge($newIndexes, [$field_name => $field_value]);
      if (!isset($textFields[$field_name]) && !empty($textIndex)) {
        $this->getCollection()->dropIndex($textIndex);
        $indexDropped = true;
      }
    }

    // If there is already a field indexed and we don't have it now for indexing
    // then drop the complete index
    if (!$indexDropped) {
      foreach (array_keys($textFields) as $field_name) {
        if (!isset($newIndexes[$field_name])) {
          $this->getCollection()->dropIndex($textIndex);
          $indexDropped = true;
        }
      }
    }

    if (empty($newIndexes) && !$indexDropped && !empty($textIndex)) {
      $this->getCollection()->dropIndex($textIndex);
      $indexDropped = true;
    }

    if ((!empty($newIndexes) && $indexDropped) || empty($textIndex)) {
      $this->getCollection()->createIndex($newIndexes);
    }
  }

  private function dropIndexes($indexesToDrop) {
    foreach ($indexesToDrop as $index) {
      $this->getCollection()->dropIndex($index->getName());
    }
  }

  /**
   * @param IndexInfo[] $existingIndexes
   * @param string $fieldName
   * @param array|int $fieldValues
   * @return int
   */
  private function findMatchingOrChangedIndexKey(array $existingIndexes, string $fieldName, $fieldValues): int {
    foreach ($existingIndexes as $key => $index) {
      if ($index->getName() !== $fieldName) {
        continue;
      }

      $existingIndexKeys = $index->getKey();

      // For single-field indexes
      if (is_int($fieldValues)) {
        if (isset($existingIndexKeys[$fieldName]) && $existingIndexKeys[$fieldName] == $fieldValues) {
          return $key;
        }
      } // For compound indexes
      elseif (is_array($fieldValues)) {
        $matchingKeys = array_intersect_assoc($fieldValues, $existingIndexKeys);
        if ($matchingKeys === $fieldValues) {
          return $key;
        }
      }
    }
    return -1;
  }

  /**
   * @param string $name
   * @param array|int $value
   * @return array
   */
  private function buildNumericIndexFieldValue($name, $value) {
    return array_merge([
      $name => $value['value'] ?? $value,
    ], $value['extra'] ?? []);
  }

  protected function compile() {
    $this->condition->compile(null);
    return $this;
  }

  protected function result() {
    // If any field is being added to the entity definition,
    // pretend we don't have any data and return [] otherwise, it would
    // stop the action. We are dealing with noSQL which is schemaless,
    // in this case, Drupal should not care about data stored previously
    if ($_POST && (isset($_POST['op']) && $_POST['op'] === 'Save field settings')) {
      return [];
    }

    /** @var \MongoDB\Collection $collection */
    $collection = $this->getCollection();

    if ($this->count) {
      return $collection->countDocuments($this->filters, $this->options);
    }

    $documents = $collection->find($this->filters, $this->options)->toArray();

    /** @var \MongoDB\Model\BSONDocument[] $result */
    $result = array_map(function ($document) {
      if ($document instanceof BSONDocument) {
        $data = $this->serializer->denormalize($document, 'mongo_to_json');
        if (!$this->returnDocuments) {
          return $data['_id']->__toString();
        } else {
          return $data;
        }
      } else {
        if (!$this->returnDocuments) {
          return $document['_id']->__toString();
        } else {
          return $document;
        }
      }
    }, $documents);

    return $result;
  }

  public function fetchAllAssoc($records, $key) {
    $return = [];
    foreach ($records as $record) {
      $record_key = is_object($record) ? $record->$key : $record[$key];
      $return[$record_key->value] = $record;
    }

    return $return;
  }


  /**
   * {@inheritdoc}
   */
  public function range($start = NULL, $length = NULL) {
    $this->count = null;

    if ($start) {
      $this->range['start'] = (int)$start;
    }

    if ($length) {
      $this->range['length'] = (int)$length;
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function count($flag = TRUE) {
    $this->range = null;

    $this->count = $flag;
    return $this;
  }


  /**
   * @return \MongoDB\Collection
   */
  public function getCollection(): \MongoDB\Collection {
    if (is_null($this->collection)) {
      // TODO: move database into settings
      $this->database = $this->factory->get('entities');
      $this->collection = $this->database->selectCollection($this->getEntityTypeId());
    }
    return $this->collection;
  }

  protected function setOptions() {
    $options = [
      'skip' => isset($this->range['start']) ? $this->range['start'] : null,
      'limit' => isset($this->range['length']) ? $this->range['length'] : null,
      'typeMap' => ['root' => 'array', 'document' => 'array']
    ];

    $this->options = array_merge($this->options, $options);

    if (!is_null($this->projection)) {
      $this->options['projection'] = $this->projection;
    }

    return $this;
  }
}

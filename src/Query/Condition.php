<?php

/**
 * @file
 * Contains \Drupal\external_entities\Entity\Query\External\Condition.
 *
 * Most of the logic copied from https://github.com/jenssegers/laravel-mongodb/blob/master/src/Jenssegers/Mongodb/Query/Builder.php#L1
 */

namespace Drupal\mcet\Query;

use Drupal\Core\Entity\Query\ConditionBase;
use Drupal\Core\Entity\Query\ConditionInterface;
use Drupal\Core\Entity\Query\QueryException;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\rabbitmq\Exception\Exception;
use MongoDB\BSON\ObjectId;

class Condition extends ConditionBase {
  /**
   * Copied from https://github.com/jenssegers/laravel-mongodb/blob/master/src/Jenssegers/Mongodb/Query/Builder.php#L1
   * Operator conversion.
   * @var array
   */
  protected $conversion = [
    '=' => '=',
    '!=' => '$ne',
    '<>' => '$ne',
    '<' => '$lt',
    '<=' => '$lte',
    '>' => '$gt',
    '>=' => '$gte',
    'IN' => '$in',
    'NOT IN' => '$nin',
    'CONTAINS' => '$search'
  ];

  public function compile($filters) {
    foreach ($this->conditions() as $condition) {
      $field = $condition['field'];
      $field_type = $this->getFieldType($field);

      // Handle field mapping for entity reference and dynamic entity reference fields
      if ($field_type === 'entity_reference' || $field_type === 'dynamic_entity_reference') {
        $field = $this->resolveReferenceField($field);
      }

      $values = $condition['value'] ?? '';
      $operator = $this->getOperator($condition['operator'] ?? "=");

      if ($operator) {
        $this->applyFieldTypeConversion($field_type, $values);

        if ($field === '_id') {
          $values = $this->convertToObjectId($values);
        }

        $filters = $this->applyOperatorFilter($field, $operator, $values);
        if (!$filters) {
          throw new Exception("Invalid operator: $operator for field: $field - value $values");
        }
      }

      $this->mergeFilter($filters);
    }
  }

  private function resolveReferenceField($field) {
    $parts = explode(".", $field);

    if (sizeof($parts) <= 1) {
      $end_field = end($parts);
      $referenced_column = ($parts[0] !== $end_field) ? $end_field : 'target_id';
      return [$parts[0], $referenced_column];
    }

    return $parts;
  }

  private function applyFieldTypeConversion($field_type, &$values) {
    $integer_types = ['integer', 'created', 'changed'];

    if (in_array($field_type, $integer_types)) {
      if (is_array($values)) {
        array_walk($values, function (&$value) {
          $value = (int)$value;
        });
      } else {
        $values = (int)$values;
      }
    }
  }

  private function convertToObjectId($values) {
    if (!is_array($values)) {
      try {
        return new ObjectId($values);
      } catch (\Exception $e) {
        watchdog_exception('mongo_content_entity_type', $e);
        return null; // Handle the error case appropriately
      }
    }

    $converted = [];

    foreach ($values as $value) {
      try {
        $converted[] = new ObjectId($value);
      } catch (\Exception $e) {
        watchdog_exception('mongo_content_entity_type', $e);
      }
    }

    return $converted;
  }

  private function applyOperatorFilter($field, $operator, $values) {
    if (in_array($operator, ['$nin', '$in'])) {
      return is_array($field) ? [$field[0] => ['$elemMatch' => [$field[1] => [$operator => array_values($values)]]]] : [$field => [$operator => array_values($values)]];
    }

    if ($operator === '$search') {
      return ['$text' => ['$search' => $values]];
    }

    if ($operator === '=') {
      return is_array($field) ? [$field[0] => implode('.', $field)] : [$field => $values];
    }

    if (in_array($operator, ['$gt', '$lt', '$ne', '$lte', '$gte'])) {
      return is_array($field) ? [$field[0] => implode('.', $field)] : [$field => [$operator => $values]];
    }

    return null;
  }


  protected function mergeFilter($filters) {
    $this->query->filters = array_merge($this->query->filters, $filters);
  }

  protected function getOperator($op) {
    $operator = isset($this->conversion[$op]) ? $this->conversion[$op] : null;
    if (is_null($operator)) {
      throw new QueryException("Operator is not supported '" . $operator . "''");
    }

    return $operator;
  }


  /**
   * @param string $field
   * @return mixed
   */
  protected function getFieldType($field_name) {
    $type = null;
    $parts = explode(".", $field_name);
    if (in_array($parts[0], array_keys($this->query->fieldDefinitions))) {
      /** @var BaseFieldDefinition $field */
      $field = $this->query->fieldDefinitions[$parts[0]];
      $type = $field->getType();
    }

    return $type;
  }

  /**
   * {@inheritdoc}
   */
  public function exists($field, $langcode = NULL) {
    return $this->condition($field, NULL, 'IS NOT NULL', $langcode);
  }

  /**
   * {@inheritdoc}
   */
  public function notExists($field, $langcode = NULL) {
    return $this->condition($field, NULL, 'IS NULL', $langcode);
  }

}

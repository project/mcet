<?php

namespace Drupal\mcet\Query;

use Drupal\Core\Entity\Query\QueryException;
use Drupal\Core\Entity\Query\QueryAggregateInterface;
use MongoDB\Model\BSONDocument;

/**
 * The mongo storage entity query aggregate class.
 */
class QueryAggregate extends Query implements QueryAggregateInterface {

  /**
   * Stores the mongo expressions used to build the sql query.
   *
   * @var array
   *   An array of expressions.
   */
  protected $expressions = [];

  /**
   * {@inheritdoc}
   */
  public function execute() {
    return $this
      ->prepare()
      ->addAggregate()
      ->result();
  }

  /**
   * {@inheritdoc}
   */
  public function conditionAggregateGroupFactory($conjunction = 'AND') {
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function existsAggregate($field, $function, $langcode = NULL) {
    return $this->conditionAggregate->exists($field, $function, $langcode);
  }

  /**
   * {@inheritdoc}
   */
  public function notExistsAggregate($field, $function, $langcode = NULL) {
    return $this->conditionAggregate->notExists($field, $function, $langcode);
  }

  /**
   * Adds the aggregations to the query.
   *
   * @return QueryAggregate
   *   Returns the called object.
   */
  protected function addAggregate() {
    if ($this->aggregate) {
      foreach ($this->aggregate as $aggregate) {
        $this->expressions[] = [
          $aggregate['stage'] => $aggregate['logic']
        ];
      }
    }
    return $this;
  }

  /**
   * @return array|int|BSONDocument[]
   */
  protected function result() {
    /** @var \MongoDB\Collection $collection */
    $collection = $this->getCollection();
    return $collection->aggregate($this->expressions, $this->options)->toArray();
  }

}

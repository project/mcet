<?php

namespace Drupal\mcet\Normalizer;

use Drupal\serialization\Normalizer\NormalizerBase as SerializationNormalizerBase;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use MongoDB\BSON\ObjectId;

/**
 * Base normalizer used in all JSON:API normalizers.
 *
 * @internal
 */
final class Normalizer extends SerializationNormalizerBase implements DenormalizerInterface {
  /**
   * The formats that the Normalizer can handle.
   *
   * @var array
   */
  protected $formats = ['mongo_to_json'];

  /**
   * {@inheritdoc}
   */
  public function normalize($document, $format = NULL, array $context = []) {
    return null;
  }

  public function denormalize($data, $class, $format = NULL, array $context = []) {
    $result = null;
    if ($data instanceof \MongoDB\Model\BSONDocument) {
      $jsonPHP = \MongoDB\BSON\fromPHP($data);
      $json = \MongoDB\BSON\toJSON($jsonPHP);
      $result = \GuzzleHttp\json_decode($json, true);
    } else {
      if (is_array($data)) {
        $result = $data;
      }
    }

    if (isset($result['_id'])) {
      if ($result['_id'] instanceof ObjectId) {
        $result['_id'] = [[
          'value' => $result['_id']->__toString()
        ]];
      } else {
        $mongo_id = new ObjectId($result['_id']['$oid']);
        $result['_id'] = [[
          'value' => $mongo_id->__toString()
        ]];
      }
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function supportsNormalization($data, $format = NULL) {
    return in_array($format, $this->formats, TRUE) && parent::supportsNormalization($data, $format);
  }

  /**
   * {@inheritdoc}
   */
  public function supportsDenormalization($data, $type, $format = NULL) {
    if ($data instanceof \MongoDB\Model\BSONDocument) {
      return TRUE;
    }

    if ($type === 'mongo_to_json' && is_array($data))
      return TRUE;

    return FALSE;
  }

}

<?php

/**
 * @file
 *
 */

namespace Drupal\mcet\Storage;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\ContentEntityStorageBase;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\mcet\Query\QueryFactory;
use MongoDB\BSON\ObjectId;
use MongoDB\Driver\Exception\BulkWriteException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\ContentEntityStorageInterface;
use MongoDB\Driver\WriteConcern;

/**
 * This extends the base storage class, adding required special handling for
 * mongo entities.
 */
class MongoContentEntityTypeStorage extends ContentEntityStorageBase implements ContentEntityStorageInterface {

  /**
   * @var \Drupal\mcet\QueryFactory
   */
  protected $queryService;

  /**
   * Constructs a new ExternalEntityStorage object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend to be used.
   * @param \Drupal\mcet\Query\QueryFactory $queryService
   */
  public function __construct(EntityTypeInterface $entity_type, EntityManagerInterface $entity_manager, CacheBackendInterface $cache, QueryFactory $queryService) {
    parent::__construct($entity_type, $entity_manager, $cache);
    $this->queryService = $queryService;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity.manager'),
      $container->get('cache.entity'),
      $container->get('mcet.query_factory')
    );
  }

  protected function getLoggerContext($msg = null) {
    return $this->database . ':' . $this->collection . '::' . $msg;
  }

  /**
   * Performs presave entity processing.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The saved entity.
   *
   * @return int|string
   *   The processed entity identifier.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   If the entity identifier is invalid.
   */
  protected function doPreSave($entity) {
    $id = parent::doPreSave($entity);
    if (!$id) {
      $id = (new ObjectId())->__toString();
      $entity->_id = $id;
    }

    return $id;
  }

  /**
   * Acts on entities before they are deleted and before hooks are invoked.
   *
   * Used before the entities are deleted and before invoking the delete hook.
   *
   * @param \Drupal\Core\Entity\EntityInterface[] $entities
   *   An array of entities.
   *
   * @throws EntityStorageException
   */
  public function preDelete(array $entities) {
    // pass
  }

  /**
   * {@inheritdoc}
   */
  protected function doDelete($entities) {
    if (!empty($entities)) {
      $ids = [];
      array_map(function ($entity) use (&$ids) {
        /** @var \Drupal\mcet\Entity\MongoContentEntityBaseInterface $entity */

        $ids[] = $entity->mongoId();
      }, $entities);

      try {
        if (!empty($ids)) {
          $result = $this->getCollection()->deleteMany([
            '_id' => ['$in' => $ids]
          ], ['writeConcern' => $this->getWriteConcern()]);
        }
      } catch (BulkWriteException $e) {
        throw $e;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function doLoadMultiple(array $ids = NULL) {
    $entities_from_cache = $this->getFromPersistentCache($ids);

    // Load any remaining entities from the database.
    if ($entities_from_storage = $this->getFromStorage($ids)) {
      $this->invokeStorageLoadHook($entities_from_storage);
      $this->setPersistentCache($entities_from_storage);
    }

    return $entities_from_cache + $entities_from_storage;
  }

  protected function getFromStorage($ids) {
    if (empty($ids))
      return [];

    $query = \Drupal::entityQuery($this->getEntityTypeId())
      ->condition('_id', $ids, 'IN');

    $documents = $query->fetchDocuments()->execute();
    $entities = $this->getDocumentEntities($documents);

    return $entities;
  }


  /**
   * @param \MongoDB\Model\BSONDocument[] $documents
   * @return \Drupal\Core\Entity\EntityTypeInterface[]
   */
  protected function getDocumentEntities($documents) {
    $context = new RenderContext();
    $query_cacheability = new CacheableMetadata();

    $storage = $this;
    $data = \Drupal::service('renderer')->executeInRenderContext($context, function () use ($storage, $documents) {
      $entities = array_map(function ($document) use ($storage) {
        $values = $storage->queryService->serializer->denormalize($document, 'mongo_to_json');

        $langcode = LanguageInterface::LANGCODE_DEFAULT;
        $langValues = array_map(function ($value) use ($langcode) {
          return [
            $langcode => $value
          ];
        }, $values);

        $entity = new $this->entityClass($langValues, $this->entityTypeId, $this->entityTypeId);
        $this->initializeValues($entity, $values);

        return $entity;
      }, $documents);

      $entities = $this->getQuery('AND')->fetchAllAssoc($entities, $this->idKey);
      return $entities;
    });

    if (!$context->isEmpty()) {
      $query_cacheability->addCacheableDependency($context->pop());
    }

    return $data;
  }

  protected function initializeValues(\Drupal\Core\Entity\EntityInterface $entity, $values) {
    foreach ($entity as $field => $value) {
      /** @var \Drupal\Core\Field\FieldItemList $value */

      $value_keys = array_keys($values);
      if (!in_array($field, $value_keys)) {
        continue;
      }

      $v = $values[$field];
      if ($value instanceof \Drupal\Core\Field\MapFieldItemList) {
        $entity->set($field, ['value' => $v]);
      } elseif (isset($values[$field])) {
        $entity->$field = $v;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function loadByProperties(array $values = []) {
    // Build a query to fetch the entity IDs.
    $entity_query = $this->getQuery();
    $entity_query->accessCheck(FALSE);
    $this->buildPropertyQuery($entity_query, $values);
    $result = $entity_query->execute();
    return $result ? $this->loadMultiple($result) : [];
  }

  /**
   * {@inheritdoc}
   */
  public function load($id) {
    $entities = $this->loadMultiple([$id]);
    return isset($entities[$id]) ? $entities[$id] : NULL;
  }

  protected function denormalizeToJson($document) {
    $json = $this->queryService->serializer->denormalize($document, 'mongo_to_json');
    return $json;
  }

  protected function normalizeEntity(\Drupal\Core\Entity\EntityInterface $entity) {
    $data = $this->queryService->serializer->normalize((clone $entity), 'json');

    $entity_type = $entity->getEntityTypeId();
    $bundle = $entity->bundle();

    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity->getEntityTypeId(), $entity->bundle());
    foreach ($fields as $name => $field) {
      if ($field->isComputed() && isset($data[$name])) {
        unset($data[$name]);
        continue;
      }

      $non_flat_types = ['entity_reference', 'file'];
      if (!in_array($field->getType(), $non_flat_types) && !empty($data[$name])) {
        if (in_array($field->getType(), ['list_string'])) {
          // walk through the list and flatten the values
          $data[$name] = array_map(function ($value) {
            return $value['value'];
          }, $data[$name]);
        } else {
          $data[$name] = $data[$name][0]['value'];
        }
      } else {
        $data[$name] = $field->getInitialValueFromField();
      }

    }
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  protected function doSave($id, \Drupal\Core\Entity\EntityInterface $entity) {
    if ($entity->isNew() && is_null($id)) {
      // Ensure the entity is still seen as new after assigning it an id, while
      // storing its data.
      $entity->enforceIsNew();
      if ($this->entityType->isRevisionable()) {
        $entity->setNewRevision();
      }
      $return = SAVED_NEW;
    } else {
      // @todo Consider returning a different value when saving a non-default
      //   entity revision. See https://www.drupal.org/node/2509360.
      $return = $entity->isDefaultRevision() ? SAVED_UPDATED : FALSE;
    }

    $context = new RenderContext();
    $query_cacheability = new CacheableMetadata();
    \Drupal::service('renderer')->executeInRenderContext($context, function () use ($entity, $id) {
      $this->saveDocumentFields($entity, $id);
    });

    if (!$context->isEmpty()) {
      $query_cacheability->addCacheableDependency($context->pop());
    }

    return $return;
  }

  /**
   * @return \MongoDB\Collection
   */
  public function getCollection() {
    /** @var \MongoDb\Database $database */
    $database = $this->queryService->getDatabase(QueryFactory::DB_NAME);
    return $database->selectCollection($this->getEntityTypeId());
  }

  protected function saveDocumentFields(ContentEntityInterface &$entity, $id) {
    if (!is_null($id)) {
      $id = new ObjectId($id);
    }

    /** @var \MongoDb\Database $database */
    $collection = $this->getCollection();

    $json = $this->normalizeEntity($entity);
    if ($json['_id']) {
      unset($json['_id']);
    }

    $is_new = $entity->isNew();
    if ($is_new) {
      /** @var \MongoDB\InsertOneResult $result */
      $json['_id'] = $id;
      $result = $collection->insertOne($json, ['writeConcern' => $this->getWriteConcern()]);
    } else {
      $collection->replaceOne(['_id' => $id], $json, ['writeConcern' => $this->getWriteConcern()]);
    }

    $data = $collection->findOne([
      '_id' => $id
    ]);

    $values = $this->denormalizeToJson($data);
    // Assigning new values to the entity object
    $entity = new $this->entityClass($values, $this->entityTypeId, $this->entityTypeId);
    $this->initializeValues($entity, $values);
  }

  /**
   * {@inheritdoc}
   */
  protected function getQueryServiceName() {
    return 'mcet.query_factory';
  }

  /**
   * {@inheritdoc}
   */
  protected function has($id, \Drupal\Core\Entity\EntityInterface $entity) {
    return !$entity->isNew();
  }

  /**
   * {@inheritdoc}
   */
  protected function doDeleteFieldItems($entities) {
  }

  /**
   * {@inheritdoc}
   */
  protected function doDeleteRevisionFieldItems(ContentEntityInterface $revision) {
  }

  /**
   * {@inheritdoc}
   */
  protected function doLoadRevisionFieldItems($revision_id) {
  }

  /**
   * {@inheritdoc}
   */
  protected function doSaveFieldItems(ContentEntityInterface $entity, array $names = array()) {
  }

  /**
   * {@inheritdoc}
   */
  protected function readFieldItemsToPurge(FieldDefinitionInterface $field_definition, $batch_size) {
    return array();
  }

  /**
   * {@inheritdoc}
   */
  protected function purgeFieldItems(ContentEntityInterface $entity, FieldDefinitionInterface $field_definition) {
  }

  /**
   * {@inheritdoc}
   */
  public function countFieldData($storage_definition, $as_bool = FALSE) {
    return $as_bool ? 0 : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function hasData() {
    return FALSE;
  }

  public function getWriteConcern() {
    return new WriteConcern(1, 1000, true);
  }

}

## A Drupal module
To support for content entities to be stored in mongo.

## dependencies
* [mongodb](https://www.drupal.org/project/mongodb)

## Usage
1. Pick an existing content entity OR create a new one just like how you would create normally
2. Instead extend the entity storage from ``Drupal\mcet\Storage\MongoContentEntityTypeStorage``
3. Instead extend the entity class from ``Drupal\mcet\Entity\MongoContentEntityBase``
4. Also extend the entity interface from ``Drupal\mcet\Entity\MongoContentEntityBaseInterface``
5. And remove ```views_data``` class as Views expects SQL Content Storage class, therefore Views is not yet supported.
6. If using forms, extend ```MongoHtmlRouteProvider``` instead of ```AdminHtmlRouteProvider``` which allows string based ids in the URL


## Setup content entities database
While setting up the main mongo module configuration, add the database name to the list of databases called "entities"
```
$settings['mongodb'] = [
  'clients' => [
    ....
  ],
  'databases' => [
    .....
    'entities' => ['default', "YOUR_DATABASE_NAME"],
  ],
];
```

## Entity id key
Change "id" to "_id" for id-key
````
 *   entity_keys = {
 *     "id" = "_id",
 *     ...
 *     ...
 *   },
````
## Mongodb indexes
Add fields info within the entity annotation like so
````
 *   mongo_indexes_fields = {
 *      "string_indexes" = {
 *          "name.value" = "text",
 *          "field_description.value" = "text"
 *      },
 *      "numeric_indexes" = {
 *          "pid" = {
 *                "value" = -1,
 *                "extra" = {
 *                    "unique" = 1
 *                 }
 *            },
 *          "created" = -1,
 *          "field_ip_address" = 1
 *     }
 *     "compound_indexes" = {
 *        "cmp_1" = {
 *          "fields" = {
 *              "user_id" = -1,
 *              "source_id" = 1
 *          }
 *        }
 *     }
 *   }
````

